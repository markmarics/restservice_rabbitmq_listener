using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestService.Entities;

namespace RestService.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly List<User> users = new()
        {
            new User { Id = Guid.NewGuid(), Name = "Michael Jackson", BirthDate = new DateTime(1958, 8, 29) },
            new User { Id = Guid.NewGuid(), Name = "Albert Einstein", BirthDate = new DateTime(1879, 3, 14) },
            new User { Id = Guid.NewGuid(), Name = "David Beckham", BirthDate = new DateTime(1975, 5, 2) },
        };
        public async Task CreateUserAsync(User user)
        {
            users.Add(user);
            await Task.CompletedTask;
        }

        public async Task DeleteUserAsync(Guid id)
        {
            var index = users.FindIndex(existingUser => existingUser.Id == id);
            users.RemoveAt(index);
            await Task.CompletedTask;
        }

        public async Task<User> GetUserAsync(Guid id)
        {
            var user = users.Where(user => user.Id == id).SingleOrDefault();
            return await Task.FromResult(user);
        }

        public async Task<IEnumerable<User>> GetUsersAsync()
        {
            return await Task.FromResult(users);
        }

        public async Task UpdateUserAsync(User user)
        {
            var index = users.FindIndex(existingUser => existingUser.Id == user.Id);
            users[index] = user;
            await Task.CompletedTask;
        }
    }
}