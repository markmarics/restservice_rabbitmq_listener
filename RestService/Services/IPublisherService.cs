namespace RestService.Services
{
    public interface IPublisherService
    {
        bool Publish(string message);

    }
}