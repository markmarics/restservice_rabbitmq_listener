using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestService.Dtos;
using RestService.Entities;
using RestService.Repositories;
using RestService.Services;

namespace RestService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUsersRepository _repository;
        private readonly IPublisherService _publisherService;
        public UsersController(IUsersRepository repository, IPublisherService publisherService)
        {
            _repository = repository;
            _publisherService = publisherService;
        }
        /// <summary>
        /// Gets all users from the in-memory database.
        /// </summary>
        /// <returns>List of users.</returns>
        /// <response code="200">Returns the users</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IEnumerable<UserDto>> GetUsersAsync()
        {
            return (await _repository.GetUsersAsync()).Select(user => user.AsDto());
        }
        /// <summary>
        /// Gets a user by the user Id.
        /// </summary>
        /// <param name="id">Id of the user.</param>
        /// <returns>404 - Not found if the user was not found with the provided Id, 200 - Success with the required user.</returns>
        /// <response code="200">Returns the user</response>
        /// <response code="404">If the user was not found</response> 
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<UserDto>> GetUserAsync(Guid id)
        {
            var user = await _repository.GetUserAsync(id);

            if (user is null)
            {
                return NotFound();
            }
            return user.AsDto();

        }
        /// <summary>
        /// Creates a new user by providing a name and a birthday.
        /// </summary>
        /// <param name="userDto">The model, which contains the new name and birthday (format: "yyyy-mm-dd").</param>
        /// <returns>A newly created user</returns>
        /// <response code="201">Returns the newly created user</response>
        /// <response code="400">If user was not created</response> 
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<User>> CreateUserAsync(CreateUserDto userDto)
        {
            User user = new()
            {
                Id = Guid.NewGuid(),
                BirthDate = userDto.BirthDate.Value,
                Name = userDto.Name
            };
            await _repository.CreateUserAsync(user);

            var message = JsonSerializer.Serialize(user);
            _publisherService.Publish(message);
            return CreatedAtAction(nameof(GetUserAsync), new { id = user.Id }, user);
        }
        /// <summary>
        /// Updates the name and birthday of a user, by the user's id.
        /// </summary>
        /// <param name="id">Id of the user.</param>
        /// <param name="userDto">The model, which contains the new name and birthday (format: "yyyy-mm-dd").</param>
        /// <returns>404 - Not found if the user was not found with the provided Id, 204 - No content if the update was successful.</returns>
        /// <response code="204">If the update was successful</response>
        /// <response code="404">If the user was not found</response> 
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> UpdateUserAsync(Guid id, UpdateUserDto userDto)
        {
            var existingUser = await _repository.GetUserAsync(id);

            if (existingUser is null)
            {
                return NotFound();
            }

            existingUser.Name = userDto.Name;
            existingUser.BirthDate = userDto.BirthDate;

            await _repository.UpdateUserAsync(existingUser);

            return NoContent();
        }
        /// <summary>
        /// Deletes a user, by the providing the user's id.
        /// </summary>
        /// <param name="id">Id of the user.</param>
        /// <returns>404 - Not found if the user was not found with the provided Id, 204 - No content if the delete was successful.</returns>
        /// <response code="204">If the delete was successful</response>
        /// <response code="404">If the user was not found</response> 
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteUserAsync(Guid id)
        {
            var existingUser = await _repository.GetUserAsync(id);

            if (existingUser is null)
            {
                return NotFound();
            }

            await _repository.DeleteUserAsync(id);

            return NoContent();
        }
    }
}