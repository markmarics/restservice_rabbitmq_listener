using RestService.Dtos;
using RestService.Entities;

namespace RestService
{
    public static class Extensions
    {
        public static UserDto AsDto(this User user)
        {
            return new UserDto
            {
                Id = user.Id,
                BirthDate = user.BirthDate,
                Name = user.Name
            };
        }
    }
}