using ListenerService.Services;
using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;

namespace ListenerService
{
    public class Worker : BackgroundService
    {
        private readonly IConsumerService _consumerService;

        public Worker(IConsumerService consumerService)
        {
            _consumerService = consumerService;
        }

        protected override Task ExecuteAsync(CancellationToken cancellationToken)
        {
            _consumerService.Subscribe();
            return Task.CompletedTask;
        }

    }
}
