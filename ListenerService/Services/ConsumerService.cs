using System;
using System.Text;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Polly;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;

namespace ListenerService.Services
{
    public class ConsumerService : IConsumerService
    {
        ConnectionFactory _factory;
        private const string ROUTINGKEY = "user.*";
        private const string EXCHANGE = "user_exchange";
        private const string QUEUE = "user_queue";
        IConnection _conn;
        private const int RETRYCOUNT = 5;
        IModel _channel;
        private readonly ILogger<ConsumerService> _logger;
        public ConsumerService(ILogger<ConsumerService> logger, IHostApplicationLifetime applicationLifetime)
        {
            _logger = logger;

            _factory = new ConnectionFactory() { HostName = "rabbitmq", Port = 5672 };
            _factory.AutomaticRecoveryEnabled = true;
            var retry = Policy
           .Handle<BrokerUnreachableException>()
           .WaitAndRetry(RETRYCOUNT, retryAttempt => TimeSpan.FromSeconds(Math.Pow(RETRYCOUNT, retryAttempt)));
            try
            {
                retry.Execute(() =>
                {
                    _logger.LogInformation($"ListenerService tries to connect to RabbitMQ at {DateTime.Now:yyyy-MM-dd HH:mm:ss}.");

                    _conn = _factory.CreateConnection();

                    _channel = _conn.CreateModel();
                    _channel.ExchangeDeclare(exchange: EXCHANGE, type:
                        ExchangeType.Topic);
                    _channel.QueueDeclare(QUEUE,
                       durable: true,
                       exclusive: false,
                       autoDelete: false,
                       arguments: null);
                    _channel.QueueBind(QUEUE, EXCHANGE, ROUTINGKEY);
                });
            }
            catch (BrokerUnreachableException)
            {
                _logger.LogError($"After {RETRYCOUNT} retries ListenerService could not connect to RabbitMQ. ListenerService stops.");
                applicationLifetime.StopApplication();
            }
            _logger.LogInformation("The connection was successful from ListenerService to RabbitMQ.");
        }

        public void Subscribe()
        {
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (sender, e) =>
            {
                var body = e.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                _logger.Log(LogLevel.Information, $"Received user creation: {message}");
            };

            _channel.BasicConsume(QUEUE, true, consumer);
        }
    }
}