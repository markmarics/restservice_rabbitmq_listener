using System;
using System.ComponentModel.DataAnnotations;

namespace RestService.Dtos
{
    public record CreateUserDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime? BirthDate { get; set; }
    }
}