using System;
using System.ComponentModel.DataAnnotations;

namespace RestService.Dtos
{
    public record UpdateUserDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime BirthDate { get; set; }
    }
}