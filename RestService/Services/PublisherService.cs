using System;
using System.Text;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Polly;
using RabbitMQ.Client;
using RabbitMQ.Client.Exceptions;

namespace RestService.Services
{
    public class PublisherService : IPublisherService
    {
        private const string EXCHANGE = "user_exchange";
        private const string ROUTINGKEY = "user.created";
        ConnectionFactory _factory;
        IModel _channel;
        IConnection _conn;
        private const int RETRYCOUNT = 5;
        private readonly ILogger<PublisherService> _logger;
        public PublisherService(ILogger<PublisherService> logger, IHostApplicationLifetime applicationLifetime)
        {
            _logger = logger;
            _factory = new ConnectionFactory() { HostName = "rabbitmq", Port = 5672 };
            _factory.AutomaticRecoveryEnabled = true;
            var retry = Policy
            .Handle<BrokerUnreachableException>()
            .WaitAndRetry(RETRYCOUNT, retryAttempt => TimeSpan.FromSeconds(Math.Pow(RETRYCOUNT, retryAttempt)));
            try
            {
                retry.Execute(() =>
                {
                    _logger.LogInformation($"RestService tries to connect to RabbitMQ at {DateTime.Now:yyyy-MM-dd HH:mm:ss}.");
                    _conn = _factory.CreateConnection();
                    _channel = _conn.CreateModel();
                    _channel.ExchangeDeclare(exchange: EXCHANGE, type:
                        ExchangeType.Topic);
                });
            }
            catch (BrokerUnreachableException)
            {
                _logger.LogError($"After {RETRYCOUNT} retries RestService could not connect to RabbitMQ. RestService stops.");
                applicationLifetime.StopApplication();
            }
            _logger.LogInformation("The connection was successful from RestService to RabbitMQ.");
        }
        public bool Publish(string message)
        {
            try
            {
                var body = Encoding.UTF8.GetBytes(message);

                _channel.BasicPublish(EXCHANGE, ROUTINGKEY, null, body);
                _logger.LogInformation($"Created user: {message}, published message to RabbitMQ.");
            }
            catch (AlreadyClosedException e)
            {
                _logger.LogError(e,"Coult not send message to RabbitMQ, because it is not running.");
                return false;
            }
            return false;
        }
    }
}