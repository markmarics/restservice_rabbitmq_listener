namespace ListenerService.Services
{
    public interface IConsumerService
    {
        void Subscribe();

    }
}